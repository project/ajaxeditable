
ajaxeditable = function() {
  var modulePath= Drupal_ajaxeditable_path;
  
//  $('div.ajaxeditable>*').css( 'opacity', '0.3' ); 
  
  $('div.ajaxeditable').filter('.noonload').each(ajaxchanger);
                                                
  $('div.ajaxeditable').not('.noonload').each(function() {
                               // spinning graphic overlay
//                               var wait_image_size = 64;
//                               var left = ( $(this).width() / 2 ) - ( wait_image_size / 2 );
//                               var top = ( $(this).height() / 2 ) - ( wait_image_size / 2 );
//                               $(this).prepend('<div style="position:relative"><div style="position:absolute; left:' + left +'px; top:' + top +'px; "><img src="' + modulePath + '/loading.gif" /></div></div>');

                               $(this).children().css( 'opacity', '0.3' ); 
                               $(this).load("/ajaxeditable_html/"+$(this).attr("nid")+"/"+$(this).attr("field"),null,ajaxchanger);
                             });
};


  
var updateValue;

ajaxupdater = function(element) 
{
  var nid=$(element).attr("nid");
  $(element).children().css( 'opacity', '0.3' ); 

  $.ajax({
  type: "POST",
        url: "/ajaxeditable_submit",
        data: $(element).serialize()+"&nid="+nid,
        element: $(element),
        success: function(msg){
            updateValue=0;
//            $(this.element).css( 'opacity', '1' ); 
          $(this.element).load("/ajaxeditable_html/"+$(this.element).attr("nid")+"/"+$(this.element).attr("field"),null,ajaxchanger);
      },
        error: function(msg){
        alert( "Error, unable to make update: " + msg.responseText );
      }
    });
};


ajaxchanger = function() 
{
//  $('div.ajaxeditable>*').each(function() {
                                 $(this).change(function() {
                                                  newValue=$(this).serialize();
                                                  if (newValue!=updateValue) {
                                                    updateValue=newValue;
                                                    ajaxupdater($(this));
                                                  }
                                                });
//                               });
};

if (Drupal.jsEnabled) {
  $(document).ready(ajaxeditable);
}
